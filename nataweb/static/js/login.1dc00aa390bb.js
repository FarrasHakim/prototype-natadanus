$(function() {
    $('#email_login_form').keyup(clearError);

    $("#loginForm").on("submit", function(event) {
        event.preventDefault();
        console.log("Data Submit");
        
        $.ajax({
            url : "/users/remote-sign-in/",
            type : "POST",
            data : {
                email : $("#email_login_form").val(),
                password : $("#pass_login_form").val(),
                csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val(),
            },
            success : function(response) {
                console.log(response == "Email or Password is invalid");
                if (response == "Email or Password is invalid") {
                    $("#login_error_message").addClass('alert alert-danger');
                    $("#login_error_message").html(response)
                } else if (response == "OK") {
                    location.reload();
                }
            }
        });

    });
});

var clearError = function() {
    $("#login_error_message").removeClass('alert alert-danger');
    $("#login_error_message").html("")
}