# Generated by Django 2.1.1 on 2019-08-26 02:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0002_auto_20190825_1106'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='status',
            field=models.CharField(choices=[('0', 'Transaksi Dibuat'), ('1', 'Menunggu Pembayaran'), ('2', 'Menunggu Konfirmasi'), ('3', 'Selesai')], default='Transaksi Dibuat', max_length=50),
        ),
    ]
