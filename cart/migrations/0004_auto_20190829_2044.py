# Generated by Django 2.1.1 on 2019-08-29 13:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0003_transaction_status'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='transaction',
            name='products',
        ),
        migrations.AlterField(
            model_name='transaction',
            name='status',
            field=models.CharField(choices=[('0', 'On Process'), ('1', 'Paid Off'), ('2', 'Confirmed Selesai')], default='On Process', max_length=50),
        ),
    ]
