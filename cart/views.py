from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import Transaction, OrderedProduct
from users.models import CustomUser
from home.models import Produk

# Create your views here.
def showCart(request):	
	userId = request.session.get("_auth_user_id", None)
	context = {}

	if userId != None:	
		userObj = CustomUser.objects.get(id=userId)
		transaction_in_queue = Transaction.objects.filter(user=userObj, status="On Process")

		if transaction_in_queue != 0:
			orderedProducts = OrderedProduct.objects.filter(user=userObj, transaction=transaction_in_queue.first())
			print(transaction_in_queue)
			print(transaction_in_queue.first())
			print(orderedProducts)

		context["orderedProducts"] = orderedProducts

		return render(request, "show-cart.html", context)
	else:
		return redirect("/home/")

def getOrCreateCart(request):
	userId = request.session.get("_auth_user_id", None)
	if userId != None:		
		userObj = CustomUser.objects.get(id=userId)	
		transactionObj = Transaction.objects.filter(user=userObj, status="On Process")	# Nyari transaksi yang belum Paid Off

		if transactionObj.count() != 1: # Kalau gaada buat transaksi baru
			transactionObj = Transaction.objects.create(user=userObj)
		else:
			transactionObj = Transaction.objects.get(user=userObj)
			print("Get transaction")
		
		if request.method == "POST":			
			produkId = request.POST.get("produkId")
			jumlahProduk = int(request.POST.get("jumlahProduk"))
			produkToBeAdded = get_object_or_404(Produk,id=produkId)
			
			# Buat ordered product untuk simpan jumlah
			orderedProduct = OrderedProduct.objects.filter(user=userObj, transaction=transactionObj, produk=produkToBeAdded) 

			if orderedProduct.count() != 1: # Kalau belum ada, buat baru dan simpan jumlah, kalau ada ganti jumlahnya saja
				orderedProduct = OrderedProduct.objects.create(user=userObj, transaction=transactionObj, produk=produkToBeAdded, jumlah=jumlahProduk)
				transactionObj.total += (produkToBeAdded.harga * jumlahProduk)
				transactionObj.save()
			else:
				print("Get ordered product")
				orderedProduct = OrderedProduct.objects.get(user=userObj, transaction=transactionObj, produk=produkToBeAdded)
				orderedProduct.jumlah += jumlahProduk
				orderedProduct.save()
				transactionObj.total += (produkToBeAdded.harga * jumlahProduk)
				transactionObj.save()

			return HttpResponse(orderedProduct.jumlah)
	else:
		return redirect("/home/")

def getItemCounter(request):
	userId = request.session.get("_auth_user_id", None)
	if userId != None:	
		print("Heyall")
		userObj = CustomUser.objects.get(id=userId)
		transaction_in_queue = Transaction.objects.filter(user=userObj, status="On Process")

		if transaction_in_queue != 0:
			orderedProducts = OrderedProduct.objects.filter(user=userObj, transaction=transaction_in_queue.first())

		itemCounter = 0
		for orderedProduct in orderedProducts:
			itemCounter += 1

		return HttpResponse(itemCounter)

	else:
		return redirect("/home/")