from django.contrib import admin
from .models import *

class TransactionAdmin(admin.ModelAdmin):
    list_display = ["user", "total"]
    list_filter = ["user"]
    search_fields = ["user"]

    class Meta:
        model = Transaction

class OrderedProductAdmin(admin.ModelAdmin):
    list_display = ["user", "produk", "jumlah"]
    list_filter = ["user", "produk"]
    search_fields = ["user", "produk"]

    class Meta:
        model = OrderedProduct

# Register your models here.
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(OrderedProduct, OrderedProductAdmin)
# Register your models here.
