from django.db import models
from users.models import CustomUser
from home.models import Produk

class Transaction(models.Model):
	status_transaksi = (
		('0', 'On Process'),
		('1', 'Paid Off'),
		('2', 'Confirmed Selesai'),
		)

	status = models.CharField(max_length=50, choices=status_transaksi, default='On Process')
	user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
	total = models.IntegerField(default=0)
	updated = models.DateTimeField(auto_now=True)
	added = models.DateTimeField(auto_now_add=True)

class OrderedProduct(models.Model):
	transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE)
	produk = models.ForeignKey(Produk, on_delete=models.CASCADE)
	user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
	jumlah = models.IntegerField(default=0)
